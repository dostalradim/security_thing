// RFID
#include <SPI.h>
#include <MFRC522.h>

// SIM800l
#include "Sim800L.h"
#include <SoftwareSerial.h>

// RFID RC5222
#define RFID_RST 9
#define RFID_SS 10
MFRC522 mfrc522(RFID_SS, RFID_RST);

// SIM800l RX+TX
#define SIM800L_RX  2 // TX na SIM800L
#define SIM800L_TX  3 // RX na SIM800L
Sim800L GSM(SIM800L_RX, SIM800L_TX);

// Management
bool DEBUG = false;
bool LOCKED = false;
bool ALARM = false;
bool NOTIFIED = false;
char* text;
char* number;

// timing
unsigned long sendingTime = 0;
const unsigned long maxTime = 4294967295;
const unsigned long waitingTimeBeforeAlarm = 5000;

// LED
const int pinGreenLed = 4;
const int pinRedLed = 5;

// DOORS
#define DOORS_NUM 1
const int pinDoors[DOORS_NUM] = {6};
bool doorsStatus[DOORS_NUM];

// TAGS
#define TAG_LEN 4
#define TAG_NUM 1
const byte tags[TAG_NUM][TAG_LEN] = {
  {0x83, 0x47, 0x75, 0x0F} // blue
};

void setup() {
  Serial.begin(9600);
  Serial.println("Zapinani");

  // DOORS
  for(int i = 0; i < DOORS_NUM; ++i) {
    pinMode(pinDoors[i], INPUT_PULLUP);
  }
  
  // LED
  pinMode(pinGreenLed, OUTPUT);
  pinMode(pinRedLed, OUTPUT);
  digitalWrite(pinGreenLed, HIGH);
  digitalWrite(pinRedLed, LOW);

  // RFID
  SPI.begin();
  mfrc522.PCD_Init();

  // SIM800l
  GSM.begin(4800);
  delay(1000);
}

void sendMessage() {
    if(DEBUG) {
      Serial.println("DEBUG: Sending message");
      return;
    }
    text = "Testing Sms";
    number = "+420XXXXXXXXX";
    GSM.sendSms(number,text);
}

// input in [ms]
unsigned long getEndTime(unsigned long timeGap) {
  unsigned long currentTime = millis();
  if((maxTime - currentTime) < timeGap) {
    return timeGap - (maxTime - currentTime);
  } else {
    return currentTime + timeGap;
  }
}

bool saveDoorsStatus() {
  // check if is possible to lock something
  bool anyClosed = false;
  for(int i = 0; i < DOORS_NUM; ++i) {
    doorsStatus[i] = digitalRead(pinDoors[i]);
    if(!anyClosed && !doorsStatus[i]) {
      anyClosed = true;
    }
  }
  return anyClosed;
}

void loop() {
  delay(500);
  // check magnets
  if(LOCKED && !ALARM) {
    for(int i = 0; i < DOORS_NUM; ++i) {
      if(!doorsStatus[i] && (digitalRead(pinDoors[i]) != doorsStatus[i])) {
        ALARM = true;
        sendingTime = getEndTime(waitingTimeBeforeAlarm);
        break;
      }
    }
  }
  if(LOCKED && ALARM && !NOTIFIED) {
    if(sendingTime < millis()) {
      sendMessage();
      NOTIFIED = true;
    }
  }

  // read tag
  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  Serial.print("UID tag: ");
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i], HEX);
  }
  Serial.println();
  // tag validating
  bool validated = false;
  for(int i = 0; i < TAG_NUM; ++i) {
    for(int j = 0; j < TAG_LEN; ++j) {
      if(tags[i][j] != mfrc522.uid.uidByte[j]) {
        validated = false;
        break;
      } else {
        validated = true;
      }
    }
  }
  if(validated) {
    if(LOCKED) {
      Serial.println("Odemykam");
      LOCKED = false;
      ALARM = false;
      NOTIFIED = false;
      digitalWrite(pinGreenLed, HIGH);
      digitalWrite(pinRedLed, LOW);
    } else if (saveDoorsStatus()) {
      Serial.println("Zamykam");
      LOCKED = true;
      digitalWrite(pinGreenLed, LOW);
      digitalWrite(pinRedLed, HIGH);
    } else {
      Serial.println("Neni mozne zamknout, zadne dvere nejsou zavreny.");
    }
  } else {
    Serial.println("Spatny tag");
  }
}
